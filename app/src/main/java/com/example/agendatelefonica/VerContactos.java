package com.example.agendatelefonica;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class VerContactos extends AppCompatActivity {
ListView listaContactos;
Button btnNuevoC;
    ArrayList<String> nombres = new ArrayList<String>();
    ArrayList<String> edades = new ArrayList<String>();
    ArrayList<String> descripciones = new ArrayList<String>();
    ArrayList<String> numeros = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_ver_contactos);

        listaContactos = (ListView) findViewById(R.id.listViewContactos);
        btnNuevoC = (Button) findViewById(R.id.btnNuevoC);

        nombres  = (ArrayList<String>)getIntent().getExtras().getStringArrayList("contactosNom");
        edades  = (ArrayList<String>)getIntent().getExtras().getStringArrayList("contactosEdad");
        descripciones  = (ArrayList<String>)getIntent().getExtras().getStringArrayList("contactosDesc");
        numeros  = (ArrayList<String>)getIntent().getExtras().getStringArrayList("contactosNum");

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, nombres);
        listaContactos.setAdapter(adapter);


        listaContactos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //se obtiene la posición del arreglo
                //en base al elemento de la lista que se haya seleccionado
                Integer posicion = position;

                Intent i = new Intent(VerContactos.this, VerDetalles.class);
                //definiendo el envío de arreglos
                i.putStringArrayListExtra( "contactosNom", nombres);
                i.putStringArrayListExtra( "contactosEdad",edades);
                i.putStringArrayListExtra( "contactosDesc",descripciones);
                i.putStringArrayListExtra( "contactosNum",numeros);
                i.putExtra("posicion", position);
                startActivity(i);
            }

        });
        btnNuevoC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}