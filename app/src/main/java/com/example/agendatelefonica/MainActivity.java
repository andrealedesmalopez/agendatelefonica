package com.example.agendatelefonica;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
EditText etxtNombre, etxtEdad, etxtDesc, etxtNumero;
Button btnAgregar, btnVerContactos;
    ArrayList<String> nombres = new ArrayList<String>();
    ArrayList<String> edades = new ArrayList<String>();
    ArrayList<String> descripciones = new ArrayList<String>();
    ArrayList<String> numeros = new ArrayList<String>();

String nombreC, edadC, descC, numeroC;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etxtNombre= (EditText)findViewById(R.id.etxtNombre);
        etxtEdad= (EditText)findViewById(R.id.etxtEdad);
        etxtDesc= (EditText)findViewById(R.id.etxtDesc);
        etxtNumero= (EditText)findViewById(R.id.etxtNumero);
        btnAgregar = (Button)findViewById(R.id.btnAgregar);
        btnVerContactos = (Button)findViewById(R.id.btnVerContactos);

        btnVerContactos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(MainActivity.this, VerContactos.class);
                //definiendo el envío de arreglos
                a.putStringArrayListExtra( "contactosNom", nombres);
                a.putStringArrayListExtra( "contactosEdad",edades);
                a.putStringArrayListExtra( "contactosDesc",descripciones);
                a.putStringArrayListExtra( "contactosNum",numeros);
                startActivity(a);

            }
        });


        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nombreC=etxtNombre.getText().toString();
                edadC=etxtEdad.getText().toString();
                descC=etxtDesc.getText().toString();
                numeroC=etxtNumero.getText().toString();

                if(nombreC.isEmpty()||edadC.isEmpty()||descC.isEmpty()||numeroC.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Ingresa todos los campos", Toast.LENGTH_LONG).show();
                }else {
                    //llenando arreglos
                   nombres.add(String.valueOf(nombreC));
                   edades.add(String.valueOf(edadC));
                   descripciones.add(String.valueOf(descC));
                   numeros.add(String.valueOf(numeroC));
                   //enviando arreglos
                    Intent i = new Intent(MainActivity.this, VerContactos.class);
                    //definiendo el envío de arreglos
                    i.putStringArrayListExtra( "contactosNom", nombres);
                    i.putStringArrayListExtra( "contactosEdad",edades);
                    i.putStringArrayListExtra( "contactosDesc",descripciones);
                    i.putStringArrayListExtra( "contactosNum",numeros);
                    startActivity(i);
                }

            }
        });
    }
}