package com.example.agendatelefonica;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static android.content.Intent.ACTION_CALL;

public class VerDetalles extends AppCompatActivity {
    ArrayList<String> nombres = new ArrayList<String>();
    ArrayList<String> edades = new ArrayList<String>();
    ArrayList<String> descripciones = new ArrayList<String>();
    ArrayList<String> numeros = new ArrayList<String>();
    TextView nombre, edad, desc, num;
    Button btnRegresar, btnLlamar;
    private final int PHONE_CALL_CODE= 100;
    ImageView fotografia;
    ImageButton btnTomarFoto;
   public String rutaImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_detalles);

        nombre = (TextView) findViewById(R.id.txtVNombre);
        edad = (TextView) findViewById(R.id.txtVEdad);
        desc = (TextView) findViewById(R.id.txtVDesc);
        num = (TextView) findViewById(R.id.txtVNum);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        btnLlamar = (Button) findViewById(R.id.btnLlamar);
        btnTomarFoto=(ImageButton)findViewById(R.id.imgbtnFotografia);
        fotografia = (ImageView) findViewById(R.id.imgFotografia);

        //llenando los arreglos nuevos con la ya registrada
        nombres = (ArrayList<String>)getIntent().getExtras().getStringArrayList("contactosNom");
        edades = (ArrayList<String>)getIntent().getExtras().getStringArrayList("contactosEdad");
        descripciones = (ArrayList<String>)getIntent().getExtras().getStringArrayList("contactosDesc");
        numeros = (ArrayList<String>)getIntent().getExtras().getStringArrayList("contactosNum");
        Integer posicion = getIntent().getExtras().getInt("posicion");
        //obteniendo la información de contacto a partir de la posición
        //seleccionada en el list view
        nombre.setText(nombres.get(posicion));
        edad.setText(edades.get(posicion));
        desc.setText(descripciones.get(posicion));
        num.setText(numeros.get(posicion));

        //tomarFotografias
        btnTomarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                abrirCamara();
            }
        }
        );


        btnLlamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String numeroC = num.getText().toString();
                if (numeroC != null) {
                    //Comprobando version actual de android
                    //versión actual>= version6
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE);

                    } else {
                        //diferencia en permisos
                        //permisos pedidos desde su instalación
                        versionesAnteriores(numeroC);

                    }
                }
            }

            private void versionesAnteriores(String num) {
                Intent intentLlamada = new Intent(ACTION_CALL, Uri.parse("tel:" + num));
                if (verificarPermisos(Manifest.permission.CALL_PHONE)) {
                    startActivity(intentLlamada);
                } else {
                    Toast.makeText(VerDetalles.this, "Configura los permisos", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PHONE_CALL_CODE:
                String permission = permissions[0];
                int result = grantResults[0];
                if (permission.equals(Manifest.permission.CALL_PHONE)){
                    // comprobación de la petición para el permiso
                    if (result == PackageManager.PERMISSION_GRANTED){
                        //permiso aceptado
                        String phoneNumber = num.getText().toString();
                        Intent llamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+phoneNumber));
                        if(ActivityCompat.checkSelfPermission(this,Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)return;
                        startActivity(llamada);
                        Toast.makeText(this, "App" , Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(this,"No aceptaste el permiso", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private boolean verificarPermisos(String permiso){
        int resultado=this.checkCallingOrSelfPermission(permiso);
        return resultado== PackageManager.PERMISSION_GRANTED;
    }

    public void abrirCamara(){
        Intent intent= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        if(intent.resolveActivity(getPackageManager())!= null){

                startActivityForResult(intent,1);
//        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imgBitmap = (Bitmap) extras.get("data") ;
            fotografia.setImageBitmap(imgBitmap);
        }
    }



}